import pygame
import sys
import random
import math


pygame.init() 

width = 600
height = 1000
screen = pygame.display.set_mode((width, height)) 
pygame.display.set_caption("Stage 1")#create pygame surface

#variables
radius = 10
gravity = 0.2


#colors
green = (0,255,0)



lines = []


#class for walls
class Wall:
    def __init__(self, pos1, pos2, color):
        self.pos1 = pos1
        self.pos2 = pos2
        self.color = color

    def draw(self, screen):
        pygame.draw.line(screen, self.color, self.pos1, self.pos2)


    def hitlines(self):
        if abs(self.pos2[0] - self.pos1[0]) < 0.0174533 :
            angle = math.pi / 2
        else:
            angle = math.atan(abs(self.pos2[1] - self.pos1[1]) / abs(self.pos2[0] - self.pos1[0]))
        angle2 = (math.pi - (angle + math.pi / 2))
        xplus = math.cos(angle2) * radius
        yplus = math.sin(angle2) * radius
        posplus = (xplus, yplus)

        if self.pos2[1] - self.pos1[1] > 0:
            nuppos1 = (self.pos1[0] + xplus, self.pos1[1] - yplus)
            nuppos2 = (self.pos2[0] + xplus, self.pos2[1] - yplus)
            ndopos1 = (self.pos1[0] - xplus, self.pos1[1] + yplus)
            ndopos2 = (self.pos2[0] - xplus, self.pos2[1] + yplus)
        elif self.pos2[1] - self.pos1[1] < 0:
            nuppos1 = (self.pos1[0] + xplus, self.pos1[1] + yplus)
            nuppos2 = (self.pos2[0] + xplus, self.pos2[1] + yplus)
            ndopos1 = (self.pos1[0] - xplus, self.pos1[1] - yplus)
            ndopos2 = (self.pos2[0] - xplus, self.pos2[1] - yplus)
        else:
            nuppos1 = (self.pos1[0] + xplus, self.pos1[1] + yplus)
            nuppos2 = (self.pos2[0] + xplus, self.pos2[1] + yplus)
            ndopos1 = (self.pos1[0] - xplus, self.pos1[1] - yplus)
            ndopos2 = (self.pos2[0] - xplus, self.pos2[1] - yplus)

        #print(angle, angle2, xplus, yplus, posplus, nuppos1, nuppos2, ndopos1, ndopos2)

        lines.append((nuppos1, nuppos2))
        lines.append((ndopos1, ndopos2))


#class for balls
class Ball:
    def __init__(self, pos, color, velocity):
        self.posx = pos[0]
        self.posy = pos[1]
        self.color = color
        self.v = list(velocity)
        self.prevposx = pos[0]
        self.prevposy = pos[1]
        


    def draw(self):
        pygame.draw.circle(screen, self.color, (self.posx, self.posy), radius)

    def gravity(self):
        print(self.v)
        self.v[1] += gravity
        print(self.v)
        self.prevposx = self.posx
        self.prevposy = self.posy
        self.posx += self.v[0]
        self.posy += self.v[1]
        print(self.prevposx, self.prevposy)
        print(self.posx, self.posy)
        

    # Nehmen wir an, du hast die Ball-Positionen (posx, posy) und den Bewegungsvektor (vx, vy)
    # und die Linie definiert durch (x1, y1) und (x2, y2)

    def check(self, x1, y1, x2, y2):

        #Ray Casting
        ray = (self.v[0], self.v[1])

      
        # Berechne den Vektor der Linie
        line_vec = (x2 - x1, y2 - y1)
        
        # Berechne den Bewegungsvektor des Balls
        ball_vec = (self.v[0], self.v[1])
        
        # Berechne den Vektor zwischen dem Startpunkt der Linie und der Ballposition
        start_to_ball = (self.posx - x1, self.posy - y1)
        
        # Projiziere die Vektoren auf eine Senkrechte zur Linie (Separating Axis Theorem)
        line_projection = line_vec[0] * start_to_ball[0] + line_vec[1] * start_to_ball[1]
        ball_projection = ball_vec[0] * start_to_ball[0] + ball_vec[1] * start_to_ball[1]
        
        # Überprüfe auf Überlappung der Projektionen, um eine mögliche Kollision zu erkennen
        if 0 <= line_projection <= (line_vec[0] * line_vec[0] + line_vec[1] * line_vec[1]) and \
           0 <= ball_projection <= (ball_vec[0] * ball_vec[0] + ball_vec[1] * ball_vec[1]):
            print("True")
            collision_point = (self.posx - self.v[0], self.posy - self.v[1])
            print(collision_point)
            collision.append(collision_point)
            return True
        else:
            return None
        

    def find_intersection_point(self, x1, y1, x2, y2, tolerance=1e-6):
        x3, y3, x4, y4 = self.prevposx, self.prevposy, self.posx, self.posy

        denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)

        if abs(denominator) < tolerance:
            return None  # Linien sind parallel oder identisch

        intersection_x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / denominator
        intersection_y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denominator

        if (min(x1, x2) - tolerance <= intersection_x <= max(x1, x2) + tolerance and
                min(y1, y2) - tolerance <= intersection_y <= max(y1, y2) + tolerance and
                min(x3, x4) - tolerance <= intersection_x <= max(x3, x4) + tolerance and
                min(y3, y4) - tolerance <= intersection_y <= max(y3, y4) + tolerance):
            collision.append((intersection_x, intersection_y))
            print(True)
            print(intersection_x, intersection_y)
            return intersection_x, intersection_y
        else:
            return None  # Schnittpunkt liegt nicht auf beiden Linien
        

    def bounce(self):
        colx = collision[0][0]
        coly = collision[0][1]
        if self.prevposx < colx:
            self.posx = colx - 1
        else:
            self.posx = colx + 1
        if self.prevposy < coly:
            self.posx = colx + 1
        else:
            self.posx = colx - 1
        collision.clear()
        print(self.v)
        self.v[0] = -0.9 * self.v[0]
        self.v[1] = -0.9 * self.v[1]
        print(self.v)
      




clock = pygame.time.Clock()

#bälle
ball1 = Ball((300,500), (255,0,0), (0,0))

#horizontale Wände
h = [
    ((100,100), (500,100)),
    ((100,900), (500,900)),
]

#vertikale Wände
v = [
    ((100,100), (100,900)),
    ((500,100), (500,900))
]

walls = []
for wall in h:
    w = Wall(wall[0], wall[1], green)
    walls.append(w)
for wall in v:
    w = Wall(wall[0], wall[1], green)
    walls.append(w)

lines = []

for wall in walls:
    wall.hitlines()

print(lines[1])

collision = []


running = True
while running:
    screen.fill((255, 255, 255))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False



    for wall in walls:
        wall.draw(screen)

    for line in lines:
        pygame.draw.line(screen, (0, 0, 255), line[0], line[1], 5)

    ball1.draw()
    ball1.gravity()

    
    for wall in lines:
        if ball1.find_intersection_point(wall[0][0], wall[0][1], wall[1][0], wall[1][1],) != None:
            ball1.bounce()
   

    pygame.display.flip()
    clock.tick(60)

pygame.quit()